import 'package:flutter/material.dart';
import 'principal.dart';
import 'registrar.dart'; 

class Login extends StatefulWidget {
  const Login({ Key? key }) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  get emailController => null;

  get onPressed => null;

  @override
  Widget build(BuildContext context) {
    var cover;
    var inputDecoration = InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Usuario o Email',
                  );
    var inputDecoration2 = inputDecoration;
    var contain;
    var scaffold = Scaffold(
        appBar: AppBar(
      
          title: Row(
          mainAxisAlignment:  MainAxisAlignment.center, 
          children: [
            Image.asset(
            'assets/logo.png', 
            
            height: 50,
            width: 50,
            ), 
            Container( 
              padding: const EdgeInsets.all(8.0),child: Text (''))
          ] 
        ), 
        backgroundColor: Color.fromRGBO(207, 21, 158, 100),
        ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              onChanged: (text) {
                print('First text field: $text');
              },
            ), 
          Padding(
            padding: const EdgeInsets.only(top:60 ),
            child: Container(
              child: RaisedButton(
                shape:RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15) ) , 
                  child: Text ("Iniciar Sesión"),
                  color: Color.fromRGBO(207, 21, 158, 100), 
                onPressed:() {
                  Navigator.push(
                    context, 
                  MaterialPageRoute(builder: (context)=> const Principal()), ); } 
              )
            ),
          ), 

          Padding(
            padding: const EdgeInsets.only(top:60 ),
            child: Container(
              child: RaisedButton(
                shape:RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15) ) , 
                  child: Text ("Registrarse"),
                  color: Color.fromRGBO(207, 21, 158, 100), 
                onPressed:() {
                  Navigator.push(
                    context, 
                  MaterialPageRoute(builder: (context)=> const Registrar()), ); } 
              )
            ),
          )


          ],
        ),
      )
    );

    return MaterialApp(
      title: 'Bienvenido',
      home: scaffold  ); }}