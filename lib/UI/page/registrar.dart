import 'package:flutter/material.dart';

class Registrar extends StatefulWidget {
  const Registrar({ Key? key }) : super(key: key);

  @override
  _RegistrarState createState() => _RegistrarState();
}

class _RegistrarState extends State<Registrar> {
  @override
  Widget build(BuildContext context) {
    var nombreController;
    return MaterialApp(
      title: 'Registro',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Registrarse'),
          backgroundColor: Color.fromRGBO(207, 21, 158, 100),
        ),
        body: Center(
          child:Column(
            children: <Widget> [
              Container(
                margin: EdgeInsets.all(20),
                child: TextField(
                  controller: nombreController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Nombre',
                  ),
                  onChanged: (text){
                    setState(() {
                    });
                  }
                ),
              )
            ],                     
          )
      ),
      )
    );
  }
}
